package pokerbots.player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Simple example pokerbot, written in Java.
 * 
 * This is an example of a bare bones, pokerbot. It only sets up the socket
 * necessary to connect with the engine and then always returns the same action.
 * It is meant as an example of how a pokerbot should communicate with the
 * engine.
 * 
 */
public class Player {
	
	private final PrintWriter outStream;
	private final BufferedReader inStream;

	public Player(PrintWriter output, BufferedReader input) {
		this.outStream = output;
		this.inStream = input;
  flopPly1 = new NeuralNetwork("resources/flopPly1");
  flopPly2 = new NeuralNetwork("resources/flopPly2");
  riverPly1 = new NeuralNetwork("resources/riverPly1");
  riverPly2 = new NeuralNetwork("resources/riverPly2");
  turnPly1 = new NeuralNetwork("resources/turnPly1");
  turnPly2 = new NeuralNetwork("resources/turnPly2");

  flopPly1.save("flopPly1");
//   flopPly2.save("resources/flopPly2");
//   riverPly1.save("resources/riverPly1");
//   riverPly2.save("resources/riverPly2");
//   turnPly1.save("resources/turnPly1");
//   turnPly2.save("resources/turnPly2");
	encoder = new StateEncoder();
	}
	
	public void run() {
		String input;
		try {
			// Block until engine sends us a packet; read it into input.
			while ((input = inStream.readLine()) != null) {

				// Here is where you should implement code to parse the packets
				// from the engine and act on it.
				System.out.println(input);
				
				String words[] = input.split(" ");
				String first_word = words[0];
				if (first_word.equals("NEWGAME")) {
					newGame(words, outStream);
				} else if (first_word.equals("KEYVALUE")) {
					keyValue(words, outStream);
				} else if (first_word.equals("REQUESTKEYVALUES")) {
					requestKeyValues(words, outStream);
				} else if (first_word.equals("NEWHAND")) {
					newHand(words, outStream);
				} else if (first_word.equals("GETACTION")) {
					getAction(words, outStream);
				} else if (first_word.equals("HANDOVER")) {
					handOver(words, outStream);
				}
			}
		} catch (IOException e) {
			System.out.println("IOException: " + e.getMessage());
		}

		System.out.println("Gameover, engine disconnected");
		
		// Once the server disconnects from us, close our streams and sockets.
		try {
			outStream.close();
			inStream.close();
		} catch (IOException e) {
			System.out.println("Encounterd problem shutting down connections");
			e.printStackTrace();
		}
	}

 	void newGame(String words[], PrintWriter output) {
 		yourName = words[1];
 		oppName = words[2];
 		startingStackSize = Integer.parseInt(words[3]);
 		bigBlind = Integer.parseInt(words[4]);
 		numHands = Integer.parseInt(words[5]);
 		timeBank = Double.parseDouble(words[6]);
  }

 void keyValue(String words[], PrintWriter output) {

 }

void requestKeyValues(String words[], PrintWriter output) {
	outStream.println("FINISH");
}

 void newHand(String words[], PrintWriter output) {
   handIndex = Integer.parseInt(words[1]);
	 onButton = Boolean.parseBoolean(words[2]);
   String cardString = "";
   String str;
   for (int i = 3; i < 7; ++i) {
     str = words[i];
     cardString += str;
   }
   hand = new CardSet(cardString);
   myStackSize = Integer.parseInt(words[7]);
   oppStackSize = Integer.parseInt(words[8]);
   timeBank = Double.parseDouble(words[9]);
   board = new CardSet();

   if (onButton) {
     preFlopCounter = 1;
     flopCounter = 2;
     riverCounter = 2;
     turnCounter = 2;
   }
   else {
     preFlopCounter = 2;
     flopCounter = 1;
     riverCounter = 1;
     turnCounter = 1;
   }
 }

 void getAction(String words[], PrintWriter stream) {
	 int index = 1;
	 int potSize = Integer.parseInt(words[index]);
	index++;
  int numBoardCards = Integer.parseInt(words[index]);
	index++;
  {
    String cardString = "";
    String str="";
    for (int i = 0; i < numBoardCards; ++ i) {
      str = words[index];
			index++;
      cardString += str;
    }
    board = new CardSet(cardString);
  }


  int numLastActions = Integer.parseInt(words[index]);
	index++;
		List<String> lastActions = new ArrayList<String>();
  for (int i = 0; i < numLastActions; ++i) {
    String action = words[index];
		index++;
    lastActions.add(action);
  }
  int numLegalActions = Integer.parseInt(words[index]);
	index++;
  List<String> legalActions = new ArrayList<String>();
  for (int i = 0; i < numLegalActions; ++i) {
    String action = words[index];
		index++;
    legalActions.add(action);
  }
  timeBank = Double.parseDouble(words[index]);

  switch (numBoardCards) {
  case 0:
		preFlop(lastActions, legalActions,  stream);
    break;
   case 3:
 		    flop(lastActions, legalActions,  stream);
     break;
	default:
		check(stream);
//   case 4:
// 		//    turn(lastActions, legalActions,  stream);
//     break;
//   case 5:
// 		//    river(lastActions, legalActions,  stream);
//     break;
  }

 }

 void handOver(String words[], PrintWriter output) {
	 int index = 1;
	 myStackSize = Integer.parseInt(words[index]);
	index++;
  oppStackSize = Integer.parseInt(words[index]);
	index++;
  int numBoardCards;
  numBoardCards = Integer.parseInt(words[index]);
	index++;
  {
    String cardString = "";
    String str;
    for (int i = 0; i < numBoardCards; ++ i) {
      str = words[index];
			index++;
      cardString += str;
    }
    board = new CardSet(cardString);
  }
  int numLastActions;
  numLastActions = Integer.parseInt(words[index]);
	index++;
  List<String> lastActions = new ArrayList<String>();
  for (int i = 0; i < numLastActions; ++i) {
    String action = words[index];
		index++;
    lastActions.add(action);
  }
  timeBank = Double.parseDouble(words[index]);
}

private	void preFlop(List<String> lastActions, List<String> legalActions, PrintWriter stream) {
  //TODO optimize ranges
   double openRange = 0.95;
   double callRange = 0.55;
   double threeBetRange = 0.15;
   double threeBetCallRange = 0.05;
//   int ranking =  handRanker.handRanking(hand);
//   std::cout << hand.str() << "\t" << ranking << std::endl;

//   if (preFlopCounter == 1) {
//     //on the button; first action
//     if (ranking < handRanker.topHands(openRange)) {
//       maxRaise(legalActions, stream);
//       //      call(stream);
//     } else {
//       fold(stream);
//     }
//   } 
//   else if (preFlopCounter == 2) {
//     if (ranking < handRanker.topHands(threeBetRange)) {
//       //call(stream);
//       maxRaise(legalActions, stream);
//     }
//     else if (ranking < handRanker.topHands(callRange)) {
//       call(stream);
//     } else {
//       fold(stream);
//     }
//   } 
//   else if (preFlopCounter == 3) {
//     if (ranking < handRanker.topHands(threeBetCallRange)) {
//       call(stream);
//     } else {
//       fold(stream);
//     }
//   } 

  call(stream);

  preFlopCounter += 2;
}

	private void flop(List<String> lastActions, List<String> legalActions, PrintWriter stream) {
  List<Double> inputArray = representation(hand, board);
  List<Double> output;
  boolean toBet = canBet(legalActions);
  if (toBet) {
    output = flopPly1.run(inputArray);
  } else {
    output = flopPly2.run(inputArray);
  }
  makeAction(legalActions, stream, output);
  flopCounter += 2;
}

	private void turn(List<String> lastActions, List<String> legalActions, PrintWriter stream) {
  List<Double> inputArray = representation(hand, board);
  List<Double> output;
  boolean toBet = canBet(legalActions);
  if (toBet) {
    output = turnPly1.run(inputArray);
  } else {
    output = turnPly2.run(inputArray);
  }
  makeAction(legalActions, stream, output);
  turnCounter += 2;
}

	private void river(List<String> lastActions, List<String> legalActions, PrintWriter stream) {
  List<Double> inputArray = representation(hand, board);
  List<Double> output;
  boolean toBet = canBet(legalActions);
  if (toBet) {
    output = riverPly1.run(inputArray);
  } else {
    output = riverPly2.run(inputArray);
  }
  makeAction(legalActions, stream, output);
  riverCounter += 2;
}

private boolean canBet(List<String> legalActions) {
  for (String str : legalActions) {
    if(str.contains("BET")) {
      return true;
    }
  }
  return false;
}

private boolean canRaise(List<String> legalActions) {
  for (String str : legalActions) {
    if(str.contains("RAISE")) {
      return true;
    }
  }
  return false;
}

private void bet(List<String> legalActions, PrintWriter stream, double increment) {
  for (String action : legalActions) {
    if(action.contains("BET")) {
			String actions[] = action.split(":");
      double minBet = Double.parseDouble(actions[1]);
      double maxBet = Double.parseDouble(actions[2]);
      int bet = (int)(Math.round(minBet + (maxBet - minBet) * increment));
      String response = "BET:" + bet;
      stream.println(response);
      return;
    }
  }
}

private void raise(List<String> legalActions, PrintWriter stream, double increment) {
  for (String action : legalActions) {
    if(action.contains("RAISE")) {
			String actions[] = action.split(":");
      double minBet = Double.parseDouble(actions[1]);
      double maxBet = Double.parseDouble(actions[2]);
      int bet = (int)(Math.round(minBet + (maxBet - minBet) * increment));
      String response = "RAISE:" + bet;
      stream.println(response);
      return;
    }
  }
}

private void check(PrintWriter output) {
  output.println("CHECK");
}

private void call(PrintWriter output) {
  output.println("CALL");
}

private void fold(PrintWriter output) {
  output.println("FOLD");
}

	private	void makeAction(List<String> legalActions, PrintWriter stream, List<Double> output) {
		if (canBet(legalActions)) {
			double checkProbability = output.get(0) / (output.get(0) + output.get(1));
			double betProbability = output.get(1) / (output.get(0) + output.get(1));
			double rand = Math.random();
			if (rand <= checkProbability) {
				check(stream);
			}
			else {
				bet(legalActions, stream, output.get(2));
			}
		}
		else {
			double foldProbability;
			double callProbability;
			double raiseProbability;
			if (canRaise(legalActions)) {
				double denominator = (output.get(0) + output.get(1) + output.get(2));
				if (denominator == 0) {
					foldProbability = 1.0/3;
				callProbability = 1.0/3;
				raiseProbability = 1.0/3;
				}
				else {
				foldProbability = output.get(0) / denominator;
				callProbability = output.get(1) / denominator;
				raiseProbability = output.get(2) / denominator;
				}
			}
			else {
				double denominator = (output.get(0) + output.get(1));
				if (denominator == 0) {
					foldProbability = 1.0/2;
				callProbability = 1.0/2;
				raiseProbability = 0;
				}
				else{
				foldProbability = output.get(0) / denominator;
				callProbability = output.get(1) / denominator;
				raiseProbability = 0;
				}
			}
			double rand = Math.random();
			if (rand <= foldProbability) {
				fold(stream);
			}
			else if (rand <= foldProbability + callProbability) {
				call(stream);
			}
			else {
				raise(legalActions, stream, output.get(3));
			}
		}
	}


List<Double> representation(CardSet myhand, CardSet myboard) {
  List<Double> response = new ArrayList<Double>();
	List<Double> result = new ArrayList<Double>();

	CardSet hand = new CardSet(myhand);
	CardSet board = new CardSet(myboard);
	
  result = encoder.fourOfAKindProbability(hand,board);
  response.add(result.get(0));
  response.add(result.get(1));
   result = encoder.fullHouseProbability(hand,board);
   response.add(result.get(0));
   response.add(result.get(1));
   result = encoder.flushProbability(hand,board);
	  response.add(result.get(0));
	    response.add(result.get(1));
   result = encoder.straightProbability(hand,board);
   response.add(result.get(0));
   response.add(result.get(1));
   result = encoder.threeOfAKindProbability(hand,board);
   response.add(result.get(0));
   response.add(result.get(1));
   result = encoder.twoPairProbability(hand,board);
   response.add(result.get(0));
   response.add(result.get(1));
   result = encoder.pairProbability(hand,board);
   response.add(result.get(0));
   response.add(result.get(1));
  return response;
}

  String yourName;
  String oppName;
  int startingStackSize;
  int bigBlind;
  int numHands;
  double timeBank;
  int handIndex;
  boolean onButton;
  CardSet hand;
  CardSet board;
  int myStackSize;
  int oppStackSize;
  int preFlopCounter;
  int flopCounter;
  int turnCounter;
  int riverCounter;
  
	NeuralNetwork flopPly1;
	NeuralNetwork flopPly2;
	NeuralNetwork riverPly1;
	NeuralNetwork riverPly2;
	NeuralNetwork turnPly1;
	NeuralNetwork turnPly2;

	StateEncoder encoder;
}
