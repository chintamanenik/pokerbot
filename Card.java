package pokerbots.player;

public class Card {
	public enum Suit {HEARTS, SPADES, CLUBS, DIAMONDS}
  public enum Rank {TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE}
	public Card(Rank rank, Suit suit) {
		myrank = rank;
		mysuit = suit;
	}

	public Card(String input) {
		myrank = rankFromString(input.charAt(0));
		mysuit = suitFromString(input.charAt(1));
	}

	public String toString(Suit suit) {
    switch (suit) {
		case CLUBS:
			return "c";
		case DIAMONDS:
			return "d";
		case HEARTS:
			return "h";
		case SPADES:
			return "s";
    }
		return "";
	}

	public String toString(Rank rank) {
    switch (rank) {
		case TWO:
			return "2";
			
		case THREE:
			return "3";
			
		case FOUR:
			return "4";
			
		case FIVE:
			return "5";
			
		case SIX:
			return "6";
			
		case SEVEN:
			return "7";
			
		case EIGHT:
			return "8";
			
		case NINE:
			return "9";
			
		case TEN:
			return "10";
			
		case JACK:
			return "J";
			
		case QUEEN:
			return "Q";
			
		case KING:
			return "K";
			
		case ACE:
			return "A";
			
    }
		return "";
	}

	public String toString() {
    return toString(myrank) + toString(mysuit);
	}

	public Suit suitFromString(char str) {
		if (str == 'c') {
			return Suit.CLUBS;
		}
		if (str == 'd') {
			return Suit.DIAMONDS;
		}
		if (str == 'h') {
			return Suit.HEARTS;
		}
		if (str == 's') {
			return Suit.SPADES;
		}
		return Suit.CLUBS;
	}

	public Rank rankFromString(char str) {
		if (str == 'A') {
			return Rank.ACE;
		}
		if (str == 'K') {
			return Rank.KING;
		}
		if (str == 'Q') {
			return Rank.QUEEN;
		}
		if (str == 'J') {
			return Rank.JACK;
		}
		if (str == 'T') {
			return Rank.TEN;
		}
		if (str == '9') {
			return Rank.NINE;
		}
		if (str == '8') {
			return Rank.EIGHT;
		}
		if (str == '7') {
			return Rank.SEVEN;
		}
		if (str == '6') {
			return Rank.SIX;
		}
		if (str == '5') {
			return Rank.FIVE;
		}
		if (str == '4') {
			return Rank.FOUR;
		}
		if (str == '3') {
			return Rank.THREE;
		}
		if (str == '2') {
			return Rank.TWO;
		}
		return Rank.TWO;
	}


	public Suit suit() {
		return mysuit;
	}
	public Rank rank() {
		return myrank;
	}

	@Override
	public int hashCode() {
		return 0;
	}

    @Override
			public boolean equals(Object obj) {
			if (!(obj instanceof Card))
				return false;
			if (obj == this)
				return true;

			Card rhs = (Card) obj;
			return mysuit == rhs.mysuit && myrank == rhs.myrank;
    }
    
	private    Suit mysuit;
	private    Rank myrank;
};
