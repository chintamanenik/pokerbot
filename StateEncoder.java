package pokerbots.player;

import java.util.*;

class StateEncoder {

	private int rankValue(Card.Rank rank) {
		switch (rank) {
		case ACE:
			return 13;
		case KING:
			return 12;
		case QUEEN:
			return 11;
		case JACK:
			return 10;
		case TEN:
			return 9;
		case NINE:
			return 8;
		case EIGHT:
			return 7;
		case SEVEN:
			return 6;
		case SIX:
			return 5;
		case FIVE:
			return 4;
		case FOUR:
			return 3;
		case THREE:
			return 2;
		case TWO:
			return 1;
		default:
			return 0;
		}
	}

	private double hasNumberOfSameSuit(CardSet board, int number, CardSet dead, Card.Suit suit, int hidden) {
		int found = 0;
		for (Card card : board.cards()) {
			if (card.suit() == suit) {
				found++;
			}
		}
		int lost = 0;
		for (Card card : dead.cards()) {
			if (card.suit() == suit) {
				lost++;
			}
		}
		int need = number - found;
		if (need < 0) {
			need = 0;
		}
		double remaining = 13 - found - lost;
		if (need > hidden) {
			return 0;
		}
		if (need == 0) {
			return 1;
		}
		else if(need == 1) {
			if (hidden == 1) {
				return remaining / (52-4-4);
			}
			else if (hidden == 2) {
				return 1 - ((52-4-3-remaining)*(52-4-4-remaining)/((52-4-3)*(52-4-4)));
			}
		}
		else if(need == 2) {
			return (remaining)*(remaining-1)/((52-4-3)*(52-4-4));
		}
		return 0;
	}

	private double hasNumberOfSameSuit(CardSet board, int number, CardSet dead, Card.Suit suit) {
		return hasNumberOfSameSuit(board, number, dead, suit, 5 - board.size());
	}


	private double hasNumberOfSameRank(CardSet board, int number, CardSet dead, Card.Rank rank, int hidden) {
		int found = 0;
		for (Card card : board.cards()) {
			if (card.rank() == rank) {
				found++;
			}
		}
		int lost = 0;
		for (Card card : dead.cards()) {
			if (card.rank() == rank) {
				lost++;
			}
		}
		int need = number - found;
		if (need < 0) {
			need = 0;
		}
		if (need > hidden) {
			return 0;
		}
		double remaining = 4 - found - lost;
		if (need == 0) {
			return 1;
		}
		else if(need == 1) {
			if (hidden == 1) {
				return remaining / (52-4-4);
			}
			else if (hidden == 2) {
				return 1 - ((52-4-3-remaining)*(52-4-4-remaining)/((52-4-3)*(52-4-4)));
			}
		}
		else if(need == 2) {
			return (remaining)*(remaining-1)/((52-4-3)*(52-4-4));
		}
		return 0;
	}

	private double hasNumberOfSameRank(CardSet board, int number, CardSet dead, Card.Rank rank) {
		return hasNumberOfSameRank(board, number, dead, rank, 5-board.size());
	}

	private double hasNumberOfSameRank(CardSet board, int number, CardSet dead) {
		double probability = 0;
		for (Card.Rank rank : ranks) {
			double prob= hasNumberOfSameRank(board, number, dead, rank);
			probability = probability + prob - probability*prob;
		}
		return probability;
	}

	public List<Double> flushProbability(CardSet hand, CardSet board) {
		double probability = 0;
		double strength = 0;
		for (Card.Suit suit : suits) {
			int numSuitInHand = 0;
			for (Card card : hand.cards()) {
				if (card.suit() == suit) {
					numSuitInHand++;
				}
			}
			int numSuitOnBoard = 0;
			for (Card card : board.cards()) {
				if (card.suit() == suit) {
					numSuitOnBoard++;
				}
			}
			if (numSuitInHand > 1) {
				double prob = hasNumberOfSameSuit(board, 3, hand, suit);
				double highestRank = 0;
				for (Card card : hand.cards()) {
					if (card.suit() == suit && rankValue(card.rank()) > highestRank) {
						highestRank = rankValue(card.rank());
					}
				}
				strength = probability * (1-prob) * strength + 
					prob * (1-probability) * highestRank/13 +
					probability * prob * Math.max(highestRank/13, strength);
				probability = prob + probability - prob*probability;
				if (probability > 0) {
					strength = strength / probability;
				}
			}
		}
		List<Double> arr = new ArrayList<Double>();
		arr.add(probability);
		arr.add(strength);
		return arr;
	}

	private double straightProbability(CardSet cards, CardSet dead, int rank1, int rank2) {
		double deadRank1 = 0;
		double deadRank2 = 0;
		if (rank1 == 0) {
			rank1 = 13;
		}
		if (rank2 == 0) {
			rank1 = 13;
		}
		if (rank1 <0 || rank1 > 13) {
			return 0;
		}
		if (rank2 <0 || rank2 > 13) {
			return 0;
		}
		for (Card card : dead.cards()) {
			if (rankValue(card.rank()) == rank1) {
				deadRank1++;
			}
			if (rankValue(card.rank()) == rank2) {
				deadRank2++;
			}
		}
		return (4-deadRank1) * (4-deadRank2) / ((52-4-3) * (52-4-4));
	}

	private List<Double> straightProbability(CardSet hand, CardSet board, int hiddenCards, CardSet dead) {
		double probability = 0;
		double strength = 0;
		List<Integer> values = new ArrayList<Integer>();
		for (Card card : hand.cards()) {
			values.add(rankValue(card.rank()));
		}
		for (Card card : board.cards()) {
			values.add(rankValue(card.rank()));
		}  
		if (hiddenCards == 0) {
			int max = Collections.max(values);
			int numbersInStraight = 0;
			if (values.contains(max)) {
				numbersInStraight++;
			}
			if (values.contains(max-1)) {
				numbersInStraight++;
			}
			if (values.contains(max-2)) {
				numbersInStraight++;
			}
			if (values.contains(max-3)) {
				numbersInStraight++;
			}
			if (values.contains(max-4)) {
				numbersInStraight++;
			}
			if(numbersInStraight == 5) {
				probability = 1;
				strength = (double)(max) / 13;
			}
			else if (values.contains(13) && values.contains(4) && values.contains(3) && values.contains(2) && values.contains(1)) {
				probability = 1;
				strength = (double)(4)/13;
			}
		}
		else if (hiddenCards == 1) {
			double successes = 0;
			double strengths = 0;
			CardSet empty = new CardSet();
			for (Card.Rank rank1: ranks) {
				for (Card.Suit suit1 : suits) {
					Card card1 = new Card(rank1, suit1);
					if (hand.contains(card1) || board.contains(card1) || dead.contains(card1)) {
						continue;
					}
					CardSet newBoard = new CardSet(board);
					newBoard.insert(card1);
					List<Double> results = straightProbability(hand, newBoard, 0, empty);
					successes += results.get(0);
					strengths += results.get(1);
				}
			}
			probability = successes / (52 - 4 - 4);
			if (probability > 0) {
				strength = strengths / successes;
			}
		}
		else if (hiddenCards == 2) {
			int max = Collections.max(values);
			int min = Collections.min(values);
			List<Integer> middleRanks = new ArrayList<Integer>();
			for (int i = min + 1; i < max; ++i) {
				if (values.contains(i)) {
					middleRanks.add(i);
				}
			}
			CardSet cards = new CardSet();
			cards.insert(hand);
			cards.insert(board);
			if (max - min == 4) {
				int missing1 = middleRanks.get(0);
				int missing2 = middleRanks.get(1);
				double prob = straightProbability(cards, dead, missing1, missing2);
				double str = (double)(max) / 13;
				strength = probability * (1-prob) * strength + 
					prob * (1-probability) * str +
					probability * prob * Math.max(str, strength);
				probability = probability + prob - probability*prob;
				if (probability > 0) {
					strength = strength / probability;
				}
			}
			if (max - min == 3) {
				{//case top
					int missing1 = middleRanks.get(0);
					int missing2 = max + 1;
					double prob = straightProbability(cards, dead, missing1, missing2);
					double str = (double)(max+1) / 13;
					strength = probability * (1-prob) * strength +
						prob * (1-probability) * str +
						probability * prob * Math.max(str, strength);
					probability = probability + prob - probability*prob;
					if (probability > 0) {
						strength = strength / probability;
					}
				}
				{//case bottom
					int missing1 = middleRanks.get(0);
					int missing2 = min - 1;
					double prob = straightProbability(cards, dead, missing1, missing2);
					double str = (double)(max) / 13;
					strength = probability * (1-prob) * strength +
						prob * (1-probability) * str +
						probability * prob * Math.max(str, strength);
					probability = probability + prob - probability*prob;
					if (probability > 0) {
						strength = strength / probability;
					}
				}
			}
			if (max - min == 2) {
				{//case top
					int missing1 = max + 2;
					int missing2 = max + 1;
					double prob = straightProbability(cards, dead, missing1, missing2);
					double str = (double)(max+2) / 13;
					strength = probability * (1-prob) * strength +
						prob * (1-probability) * str +
						probability * prob * Math.max(str, strength);
					probability = probability + prob - probability*prob;
					if (probability > 0) {
						strength = strength / probability;
					}
				}
				{//case surround
					int missing1 = max + 1;
					int missing2 = min - 1;
					double prob = straightProbability(cards, dead, missing1, missing2);
					double str = (double)(max+1) / 13;
					strength = probability * (1-prob) * strength +
						prob * (1-probability) * str +
						probability * prob * Math.max(str, strength);
					probability = probability + prob - probability*prob;
					if (probability > 0) {
						strength = strength / probability;
					}
				}
				{//case bottom
					int missing1 = min - 1;
					int missing2 = min - 2;
					double prob = straightProbability(cards, dead, missing1, missing2);
					double str = (double)(max) / 13;
					strength = probability * (1-prob) * strength +
						prob * (1-probability) * str +
						probability * prob * Math.max(str, strength);
					probability = probability + prob - probability*prob;
					if (probability > 0) {
						strength = strength / probability;
					}
				}
			}
		}
		List<Double> arr = new ArrayList<Double>();
		arr.add(probability);
		arr.add(strength);
		return arr;
	}

	public List<Double> straightProbability(CardSet hand, CardSet board) {
		double probability = 0;
		double strength = 0;
		List<CardSet> handStates = new ArrayList<CardSet>();
		List<CardSet> boardStates = new ArrayList<CardSet>();
		handStates = hand.fill(2);
		int hiddenCards = 5 - board.size();
		if (hiddenCards == 0) {
			boardStates = board.fill(3);
			for (int i = 0; i < handStates.size(); ++i) {
				CardSet deadHand = new CardSet(hand);
				deadHand.remove(handStates.get(i));
				for (int j = 0; j < boardStates.size(); ++j) {
					CardSet deadCards = new CardSet(board);
					deadCards.remove(boardStates.get(j));
					deadCards.insert(deadHand);
					List<Double> results = straightProbability(handStates.get(i), boardStates.get(j), 0, deadCards);
					double prob = results.get(0);
					strength = probability * (1-prob) * strength + 
						prob * (1-probability) * results.get(1) +
						probability * prob * Math.max(results.get(1), strength);
					probability = probability + prob - probability*prob;
					if (probability > 0) {
						strength = strength / probability;
					}
				}
			}
		}
		else if (hiddenCards == 1) {
			//full board first
			boardStates = board.fill(3);
			for (int i = 0; i < handStates.size(); ++i) {
				CardSet deadHand = new CardSet(hand);
				deadHand.remove(handStates.get(i));
				for (int j = 0; j < boardStates.size(); ++j) {
					CardSet deadCards = new CardSet(board);
					deadCards.remove(boardStates.get(j));
					deadCards.insert(deadHand);
					List<Double> results = straightProbability(handStates.get(i), boardStates.get(j), 0, deadCards);
					double prob = results.get(0);
					strength = probability * (1-prob) * strength + 
						prob * (1-probability) * results.get(1) +
						probability * prob * Math.max(results.get(1), strength);
					probability = probability + prob - probability*prob;
					if (probability > 0) {
						strength = strength / probability;
					}
				}
			}
			//missing one card next
			boardStates.clear();
			boardStates = board.fill(2);
			for (int i = 0; i < handStates.size(); ++i) {
				CardSet deadHand = new CardSet(hand);
				deadHand.remove(handStates.get(i));
				for (int j = 0; j < boardStates.size(); ++j) {
					CardSet deadCards = new CardSet(board);
					deadCards.remove(boardStates.get(j));
					deadCards.insert(deadHand);
					List<Double> results = straightProbability(handStates.get(i), boardStates.get(j), 1, deadCards);
					double prob = results.get(0);
					strength = probability * (1-prob) * strength + 
						prob * (1-probability) * results.get(1) +
						probability * prob * Math.max(results.get(1), strength);
					probability = probability + prob - probability*prob;
					if (probability > 0) {
						strength = strength / probability;
					}
				}
			}
		}
		else if (hiddenCards == 2) {
			//full board first
			boardStates= board.fill(3);
			for (int i = 0; i < handStates.size(); ++i) {
				CardSet deadHand = new CardSet(hand);
				deadHand.remove(handStates.get(i));
				for (int j = 0; j < boardStates.size(); ++j) {
					CardSet deadCards = new CardSet(board);
					deadCards.remove(boardStates.get(j));
					deadCards.insert(deadHand);
					List<Double> results = straightProbability(handStates.get(i), boardStates.get(j), 0, deadCards);
					double prob = results.get(0);
					strength = probability * (1-prob) * strength + 
						prob * (1-probability) * results.get(1) +
						probability * prob * Math.max(results.get(1), strength);
					probability = probability + prob - probability*prob;
					if (probability > 0) {
						strength = strength / probability;
					}
				}
			}
			//missing one card next
			boardStates.clear();
			boardStates = board.fill(2);
			for (int i = 0; i < handStates.size(); ++i) {
				CardSet deadHand = new CardSet(hand);
				deadHand.remove(handStates.get(i));
				for (int j = 0; j < boardStates.size(); ++j) {
					CardSet deadCards = new CardSet(board);
					deadCards.remove(boardStates.get(j));
					deadCards.insert(deadHand);
					List<Double> results = straightProbability(handStates.get(i), boardStates.get(j), 1, deadCards);
					double prob = results.get(0);
					strength = probability * (1-prob) * strength + 
						prob * (1-probability) * results.get(1) +
						probability * prob * Math.max(results.get(1), strength);
					probability = probability + prob - probability*prob;
					if (probability > 0) {
						strength = strength / probability;
					}
				}
			}
			//missing two cards next
			boardStates.clear();
			boardStates = board.fill(1);
			for (int i = 0; i < handStates.size(); ++i) {
				CardSet deadHand = new CardSet(hand);
				deadHand.remove(handStates.get(i));
				for (int j = 0; j < boardStates.size(); ++j) {
					CardSet deadCards = new CardSet(board);
					deadCards.remove(boardStates.get(j));
					deadCards.insert(deadHand);
					List<Double> results = straightProbability(handStates.get(i), boardStates.get(j), 1, deadCards);
					double prob = results.get(0);
					strength = probability * (1-prob) * strength + 
						prob * (1-probability) * results.get(1) +
						probability * prob * Math.max(results.get(1), strength);
					probability = probability + prob - probability*prob;
					if (probability > 0) {
						strength = strength / probability;
					}
				}
			}
		}
		List<Double> arr = new ArrayList<Double>();
		arr.add(probability);
		arr.add(strength);
		return arr;
	}

	private List<Double> fullHouseProbability(Card card1, Card card2, CardSet board, CardSet hand) {
		double probability = 0;
		double strength = 0;
		if (card1.rank() == card2.rank()) {
			probability = hasNumberOfSameRank(board, 3, hand);
			strength = (double) (rankValue(card1.rank())) / 13 * probability;
		}
		else {
			double flush1 = 0;
			double flush2 = 0;
			int hidden = 5 - board.size();
			for (int i = 0; i <= hidden; ++i) {
				for (int j = 0; j <= hidden; ++j) {
					if (i + j == hidden) {
						flush1 += hasNumberOfSameRank(board, 2, hand, card1.rank(), i) * hasNumberOfSameRank(board, 1, hand, card2.rank(), j);
						flush2 += hasNumberOfSameRank(board, 2, hand, card2.rank(), i) * hasNumberOfSameRank(board, 1, hand, card1.rank(), j);
					}
				}
			}
			strength = flush1*(1-flush2) * rankValue(card1.rank()) +
				flush2*(1-flush1) * rankValue(card2.rank()) +
				flush1*flush2 * Math.max(rankValue(card1.rank()), rankValue(card2.rank()));
			probability = flush1 + flush2 - flush1*flush2;
			strength /= 13;
		}
		if (probability > 0) {
			strength /= probability;
		}
		List<Double> arr = new ArrayList<Double>();
		arr.add(probability);
		arr.add(strength);
		return arr;
	}

	public List<Double> fullHouseProbability(CardSet hand, CardSet board) {
		double probability = 0;
		double strength = 0;
		for (int i=0; i < hand.cards().size() ; ++i) {
			for (int j = 0; j < i; ++j) {
				Card card1 = hand.cards().get(i);
				Card card2 = hand.cards().get(j);
				List<Double> results = fullHouseProbability(card1, card2, board, hand);
				double prob = results.get(0);
				strength = probability * (1-prob) * strength + 
					prob * (1-probability) * results.get(1) +
					probability * prob * Math.max(results.get(1), strength);
				probability = probability + prob - probability*prob;
				if (probability > 0) {
					strength = strength / probability;
				}
			}
		}
		List<Double> arr = new ArrayList<Double>();
		arr.add(probability);
		arr.add(strength);
		return arr;
	}

	private List<Double> twoPairProbability(Card card1, Card card2, CardSet board, CardSet hand) {
		double probability = 0;
		double strength = 0;
		if (card1.rank() == card2.rank()) {
			probability = hasNumberOfSameRank(board, 2, hand);
			strength = (double) (rankValue(card1.rank())) / 13 * probability;
		}
		else {
			int hidden = 5 - board.size();
			for (int i = 0; i <= hidden; ++i) {
				for (int j = 0; j <= hidden; ++j) {
					if (i + j == hidden) {
						probability += hasNumberOfSameRank(board, 1, hand, card1.rank(), i) * hasNumberOfSameRank(board, 1, hand, card2.rank(), j);
					}
				}
			}
			strength = (double) (13 * Math.max(rankValue(card1.rank()), rankValue(card2.rank())) + Math.min(rankValue(card1.rank()), rankValue(card2.rank()))) / 169 * probability;
		}
		List<Double> arr = new ArrayList<Double>();
		arr.add(probability);
		arr.add(strength);
		return arr;
	}

	public List<Double> twoPairProbability(CardSet hand, CardSet board) {
		double probability = 0;
		double strength = 0;
		for (int i=0; i < hand.cards().size() ; ++i) {
			for (int j = 0; j < i; ++j) {
				Card card1 = hand.cards().get(i);
				Card card2 = hand.cards().get(j);
				List<Double> results = twoPairProbability(card1, card2, board, hand);
				double prob = results.get(0);
				strength = probability * (1-prob) * strength + 
					prob * (1-probability) * results.get(1) +
					probability * prob * Math.max(results.get(1), strength);
				probability = probability + prob - probability*prob;
				if (probability > 0) {
					strength = strength / probability;
				}
			}
		}
		List<Double> arr = new ArrayList<Double>();
		arr.add(probability);
		arr.add(strength);
		return arr;
	}

	public List<Double> fourOfAKindProbability(CardSet hand, CardSet board) {
		double probability = 0;
		double strength = 0;
		for (Card.Rank rank : ranks) {
			int numRankInHand = 0;
			for (Card card : hand.cards()) {
				if (card.rank() == rank) {
					numRankInHand++;
				}
			}
			if (numRankInHand > 1) {
				double prob = hasNumberOfSameRank(board, 2, hand, rank);
				double str = (double) (rankValue(rank)) / 13;
				strength = probability * (1-prob) * strength + 
					prob * (1-probability) * str +
					probability * prob * Math.max(str, strength);
				probability = probability + prob - probability*prob;
				if (probability > 0) {
					strength = strength / probability;
				}
			}
			else if(numRankInHand == 1) {
				double prob = hasNumberOfSameRank(board, 3, hand, rank);
				double str = (double) (rankValue(rank)) / 13; 
				strength = probability * (1-prob) * strength + 
					prob * (1-probability) * str +
					probability * prob * Math.max(str, strength);
				probability = probability + prob - probability*prob;
				if (probability > 0) {
					strength = strength / probability;
				}
			}
		}
		List<Double> arr = new ArrayList<Double>();
		arr.add(probability);
		arr.add(strength);
		return arr;
	}

	public List<Double> threeOfAKindProbability(CardSet hand, CardSet board) {
		double probability = 0;
		double strength = 0;
		for (Card.Rank rank : ranks) {
			int numRankInHand = 0;
			for (Card card : hand.cards()) {
				if (card.rank() == rank) {
					numRankInHand++;
				}
			}
			if (numRankInHand > 1) {
				double prob = hasNumberOfSameRank(board, 1, hand, rank);
				double str = (double) (rankValue(rank)) / 13;
				strength = probability * (1-prob) * strength + 
					prob * (1-probability) * str +
					probability * prob * Math.max(str, strength);
				probability = probability + prob - probability*prob;
				if (probability > 0) {
					strength = strength / probability;
				}
			}
			else if(numRankInHand == 1) {
				double prob = hasNumberOfSameRank(board, 2, hand, rank); 
				double str = (double) (rankValue(rank)) / 13; 
				strength = probability * (1-prob) * strength + 
					prob * (1-probability) * str +
					probability * prob * Math.max(str, strength);
				probability = probability + prob - probability*prob;
				if (probability > 0) {
					strength = strength / probability;
				}
			}
			else if(numRankInHand == 0) {
				double prob = hasNumberOfSameRank(board, 3, hand, rank); 
				double str = (double) (rankValue(rank)) / 13; 
				strength = probability * (1-prob) * strength + 
					prob * (1-probability) * str +
					probability * prob * Math.max(str, strength);
				probability = probability + prob - probability*prob;
				if (probability > 0) {
					strength = strength / probability;
				}
			}
		}
		List<Double> arr = new ArrayList<Double>();
		arr.add(probability);
		arr.add(strength);
		return arr;
	}

	public List<Double> pairProbability(CardSet hand, CardSet board) {
		double probability = 0;
		double strength = 0;
		for (Card.Rank rank : ranks) {
			int numRankInHand = 0;
			for (Card card : hand.cards()) {
				if (card.rank() == rank) {
					numRankInHand++;
				}
			}
			if (numRankInHand > 1) {
				double prob = hasNumberOfSameRank(board, 0, hand, rank);
				double str = (double) (rankValue(rank)) / 13; 
				strength = probability * (1-prob) * strength + 
					prob * (1-probability) * str +
					probability * prob * Math.max(str, strength);
				probability = probability + prob - probability*prob;
				if (probability > 0) {
					strength = strength / probability;
				}
			}
			else if(numRankInHand == 1) {
				double prob = hasNumberOfSameRank(board, 1, hand, rank);
				double str = (double) (rankValue(rank)) / 13; 
				strength = probability * (1-prob) * strength + 
					prob * (1-probability) * str +
					probability * prob * Math.max(str, strength);
				probability = probability + prob - probability*prob;
				if (probability > 0) {
					strength = strength / probability;
				}
			}
			else if(numRankInHand == 0) {
				double prob = hasNumberOfSameRank(board, 2, hand, rank);
				double str = (double) (rankValue(rank)) / 13; 
				strength = probability * (1-prob) * strength + 
					prob * (1-probability) * str +
					probability * prob * Math.max(str, strength);
				probability = probability + prob - probability*prob;
				if (probability > 0) {
					strength = strength / probability;
				}
			}
		}
		List<Double> arr = new ArrayList<Double>();
		arr.add(probability);
		arr.add(strength);
		return arr;
	}

	public StateEncoder() {
		suits = new ArrayList<Card.Suit>();
		ranks = new ArrayList<Card.Rank>();
		map = new ArrayList<Card>();

		suits.add(Card.Suit.CLUBS);
		suits.add(Card.Suit.DIAMONDS);
		suits.add(Card.Suit.HEARTS);
		suits.add(Card.Suit.SPADES);

		ranks.add(Card.Rank.TWO);
		ranks.add(Card.Rank.THREE);
		ranks.add(Card.Rank.FOUR);
		ranks.add(Card.Rank.FIVE);
		ranks.add(Card.Rank.SIX);
		ranks.add(Card.Rank.SEVEN);
		ranks.add(Card.Rank.EIGHT);
		ranks.add(Card.Rank.NINE);
		ranks.add(Card.Rank.TEN);
		ranks.add(Card.Rank.JACK);
		ranks.add(Card.Rank.QUEEN);
		ranks.add(Card.Rank.KING);
		ranks.add(Card.Rank.ACE);

		for (Card.Suit suit : suits) {
			for (Card.Rank rank : ranks) {
				map.add(new Card(rank,suit));
			}
		}
	}

	List<Card> map;
	List<Card.Rank> ranks;
	List<Card.Suit> suits;
}