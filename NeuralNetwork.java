package pokerbots.player;
import java.util.*;
import java.io.*;

public class NeuralNetwork{
	public NeuralNetwork(List<Integer> lays) {
		layers = new ArrayList<Integer>(lays);
		weights = new ArrayList<Double[][]>();
		biases = new ArrayList<Double[]>();
		for (int i = 0; i < layers.size() - 1; ++i) {
			Double[][] weight = new Double[layers.get(i)][layers.get(i+1)];
			for (int row = 0; row < layers.get(i); ++row) {
				for (int col = 0; col < layers.get(i+1); ++col) {
					weight[row][col] = Math.random();
				}
			}
			weights.add(weight);
		}
		for (int i = 0; i < layers.size(); ++i) {
			Double[] bias = new Double[layers.get(i)];
			for (int j = 0; j < layers.get(i); ++j) {
				bias[j] = Math.random();
			}
			biases.add(bias);
		}
	}

	public NeuralNetwork(String file) {
		layers = new ArrayList<Integer>();
		weights = new ArrayList<Double[][]>();
		biases = new ArrayList<Double[]>();
		load(file);
	}
	
	public List<Double> run(List<Double> inputs) {
		List<Double> start = new ArrayList<Double>(inputs);
		System.out.print("INPUTS: ");
		for (double x : inputs) {
			System.out.print(x + " ");
		}
		System.out.println();
		for (int layer = 0; layer < layers.size() - 1; ++layer) {
			start = run(start, weights.get(layer), biases.get(layer), layers.get(layer), layers.get(layer+1)); 
		}
		System.out.print("OUTPUTS: ");
		List<Double> end = new ArrayList<Double>();
		for (int i = 0; i < start.size(); ++i) {
			double x = start.get(i);
			double bias = biases.get(layers.size()-1)[i];
			end.add(sigmoid(x+bias));
			System.out.print(end.get(i)+" ");
		}
		
		return end;
	}

	public void save(String file) {
		try{
		PrintWriter writer = new PrintWriter(file, "UTF-8");
		for (int i = 0; i < layers.size(); ++i) {
			writer.print(layers.get(i));
			writer.print(" ");
		}
		writer.println();

		for (int i = 0; i < layers.size() - 1; ++i) {
			Double[][] weight = weights.get(i);
			for (int row = 0; row < layers.get(i); ++row) {
				for (int col = 0; col < layers.get(i+1); ++col) {
					writer.print(weight[row][col]);
					writer.print(" ");
				}
			}
			writer.println();
		}

		for (int i = 0; i < layers.size(); ++i) {
			Double[] bias = biases.get(i);
			for (int j = 0; j < layers.get(i); ++j) {
				writer.print(bias[j]);
				writer.print(" ");
			}
			writer.println();
		}
		writer.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void load(String file) {
		try{
			FileReader inputFile = new FileReader(file);
			BufferedReader bufferReader = new BufferedReader(inputFile);
			String line;
			{
				line = bufferReader.readLine();
				String[] values = line.split(" ");
				for (String str : values) {
					layers.add(Integer.parseInt(str));
				}
			}
			for (int i = 0; i < layers.size() - 1; ++i) {
				line = bufferReader.readLine();
				String values[] = line.split(" ");
				Double[][] weight = new Double[layers.get(i)][layers.get(i+1)];
				int index = 0;
				for (int row = 0; row < layers.get(i); ++row) {
					for (int col = 0; col < layers.get(i+1); ++col) {
						weight[row][col] = Double.parseDouble(values[index]);
						index++;
					}
				}
				weights.add(weight);
			}
			for (int i = 0; i < layers.size(); ++i) {
				line = bufferReader.readLine();
				String values[] = line.split(" ");
				Double[] bias = new Double[layers.get(i)];
				int index = 0;
				for (int j = 0; j < layers.get(i); ++j) {
					bias[j] = Double.parseDouble(values[index]);
					index++;
				}
				biases.add(bias);
			}
			bufferReader.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}


	private List<Double> run(List<Double> inputs, Double[][] weights, Double[] biases, int numInputs, int numOutputs) {
		List<Double> outputs = new ArrayList<Double>();
		for (int i = 0; i < numOutputs; ++i) {
			double output = 0;
			for (int j = 0; j < numInputs; ++j) {
				double input = inputs.get(j) + biases[j];
				output += input * weights[j][i];
			}
			outputs.add(tanh(output));
		}
		return outputs;
	}

	private double sigmoid(double x) {
		return 1/(1 + Math.exp(-2*x));
	}

	private double tanh(double x) {
		return 2/(1 + Math.exp(-2*x)) - 1;
	}

	private List<Integer> layers;
	private List<Double[][]> weights;  
	private List<Double[]> biases;  
}