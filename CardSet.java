package pokerbots.player;
import java.util.*;

public class CardSet {

	public CardSet() {
		set = new ArrayList<Card>();
	}

	public CardSet(CardSet copy) {
		set = new ArrayList<Card>();
		for (Card card : copy.set) {
			set.add(card);
		}
	}

	public CardSet(String cards) {
		set = new ArrayList<Card>();
		for (int i = 0; i < cards.length(); i +=2) {
			String sub = cards.substring(i,i+2);
			set.add(new Card(sub));
		}
	}

	public List<Card> cards() {
		return set;
	}

	public List<CardSet> fill(int n) {
		List<CardSet> result = new ArrayList<CardSet>();
		if (n == 1) {
			for (int i = 0; i < set.size(); ++i) {
					CardSet hand = new CardSet();
					hand.insert(set.get(i));
					result.add(hand);
				}
		}
		else if (n == 2) {
			for (int i = 0; i < set.size(); ++i) {
				for (int j = 0; j < i; ++j) {
					CardSet hand = new CardSet();
					hand.insert(set.get(i));
					hand.insert(set.get(j));
					result.add(hand);
				}
			}
		}
		else if (n == 3) {
			for (int i = 0; i < set.size(); ++i) {
				for (int j = 0; j < i; ++j) {
					for (int k = 0; k < j; ++k) {
						CardSet hand = new CardSet();
						hand.insert(set.get(i));
						hand.insert(set.get(j));
						hand.insert(set.get(k));
						result.add(hand);
					}
				}
			}
		}
			return result;
	}

		public void insert(CardSet cards) {
			for (Card card : cards.cards()) {
				if (!set.contains(card)) {
					set.add(card);
				}
			}
		}

		public void insert(Card card) {
			if (!set.contains(card)) {
				set.add(card);
			}
		}

		public void remove(Card card) {
			set.remove(card);
		}

		public void remove(CardSet cards) {
			for (Card card: cards.cards()) {
			set.remove(card);
			}
		}

		public boolean contains(Card card) {
			return set.contains(card);
		}

		public	int size() {
			return set.size();
		}

		private List<Card> set;
	};
